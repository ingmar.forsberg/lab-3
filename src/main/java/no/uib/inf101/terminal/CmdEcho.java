package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        String newString = "";
        for(String word:args) {
            newString = newString + word + " ";
        }
        return newString;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "echo";
    }

    
}
